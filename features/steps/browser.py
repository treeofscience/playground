from behave import given, when, then


@given(u'I have a headless browser')
def i_have_a_headless_browser(context):
    assert context.browser is not None


@when(u'I visit "{page}"')
def i_visit(context, page):
    context.browser.get(page)


@then(u'I should see a single input')
def i_should_see_a_single_input(context):
    inputs = context.browser.find_elements_by_tag_name('input')
    assert 1 == len(inputs)


@then(u'I should see at least an input')
def i_should_see_at_least_an_input(context):
    inputs = context.browser.find_elements_by_tag_name('input')
    assert 1 <= len(inputs)


@then(u'I should see "{text}" in the title')
def i_should_see_in_the_title(context, text):
    assert text == context.browser.title

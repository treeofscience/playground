@browser
Feature: A good browser integration

    As a web developer
    I need a headless testing browser
    So I don't expend all my life waiting for the window to load

    Scenario: Visiting google
        Given I have a headless browser
        When I visit "http://google.com"
        Then I should see at least an input
        And I should see "Google" in the title

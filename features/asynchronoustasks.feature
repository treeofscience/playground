@browser
Feature: Asynchronous tasks

    In order to avoid waiting and browser crashes
    As a user
    I want the control of my browser even if the tasks arent finished yet

    Senario: Creating a task
        Given I'm on the tasks page
        And I set up a "5" minutes lasting task with name "example"
        When I submit the task
        Then I should see the "example" task in the task list
        And the "example" task should be marked as "in progress"

    Scenario: Monitoring a task
        Given I'm on the tasks page
        And there is a "monitoring" task lasting "1" minute
        When I click the "is it ready" button on the "monitoring" task
        Then I should see "the monitoring task is still in progress"
        And the "monitoring" task should be marked as "in progress"

    Scenario: Finishing a task
        Given I'm on the tasks page
        And there is a "finishing" task lasting "5" seconds
        When I wait for "5" seconds
        Then I should see "the finishing task has been completed"
        And the "finishing" task should be marked as "complete"

    Scenario: Finishing a task from another page
        Given I'm on the tasks page
        And there is a finishing task lasting "5" seconds
        When I visit the homepage
        And I wait for "5" seconds
        Then I should see "the finishing task has been completed"
        And I should see a link to the "finishing" task
